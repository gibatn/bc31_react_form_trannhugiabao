import "./App.css";
import FormSinhVien from "./Form/FormSinhVien";
import TableSinhVien from "./Form/Table";

function App() {
  return (
    <div className="container p-5 text-center">
      <FormSinhVien />
      <TableSinhVien />
    </div>
  );
}

export default App;
