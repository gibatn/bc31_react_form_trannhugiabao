import React, { Component } from "react";
import { connect } from "react-redux";
import { addSV, ADD_SV, getInput, updateSV } from "../Redux/action/actionSV";
import { LAY_DL } from "../Redux/type/SVType";
class FormSinhVien extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.formRef = React.createRef();
  }
  componentDidMount() {
    this.inputRef.current.focus();
  }
  render() {
    console.log(this.props.sinhVienInput);
    return (
      <div>
        <form ref={this.formRef} className="text-left p-4">
          <div className="row">
            <div className="form-group col-6">
              <label for="">Mã SV:</label>
              <input
                value={this.props.sinhVienInput.maSV}
                ref={this.inputRef}
                onChange={(e) => {
                  this.props.getInput(e.target);
                }}
                type="text"
                className="form-control"
                name
                id="maSV"
                aria-describedby="helpId"
                placeholder="Nhập mã sinh viên"
              />
            </div>
            <div className="form-group col-6">
              <label for="">Họ tên</label>
              <input
                value={this.props.sinhVienInput.tenSV}
                onChange={(e) => {
                  this.props.getInput(e.target);
                }}
                type="text"
                className="form-control"
                name
                id="tenSV"
                aria-describedby="helpId"
                placeholder="Nhập vào họ và tên"
              />
            </div>
            <div className="form-group col-6">
              <label for="">Số điện thoại</label>
              <input
                value={this.props.sinhVienInput.sdtSV}
                onChange={(e) => {
                  this.props.getInput(e.target);
                }}
                type="text"
                className="form-control"
                name
                id="sdtSV"
                aria-describedby="helpId"
                placeholder="Nhập vào số điện thoại"
              />
            </div>
            <div className="form-group col-6">
              <label for=""></label>
              <input
                value={this.props.sinhVienInput.emailSV}
                onChange={(e) => {
                  this.props.getInput(e.target);
                }}
                type="text"
                className="form-control"
                name
                id="emailSV"
                aria-describedby="helpId"
                placeholder="Nhập vào email"
              />
            </div>
            <div className="col-4"></div>
            <button
              type="button"
              onClick={() => {
                this.props.addSV(this.props.sinhVienInput);
                this.formRef.current.reset();
              }}
              className="btn btn-success ml-3 px-4"
            >
              Thêm sinh viên
            </button>
            <button
              onClick={() => {
                this.props.updateSV(this.props.sinhVienInput);
              }}
              type="button"
              className="btn btn-success ml-3"
            >
              Cập nhật sinh viên
            </button>
          </div>
        </form>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    sinhVienInput: state.formSinhVienReducer.sinhVienInput,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    getInput: (value) => {
      dispatch(getInput(value));
    },
    addSV: (payload) => {
      dispatch(addSV(payload));
    },
    updateSV: (payload) => {
      dispatch(updateSV(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormSinhVien);
