import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteSV, getInfoSV } from "../Redux/action/actionSV";

class TableSinhVien extends Component {
  renderTable = () => {
    return this.props.sinhVienArr.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.maSV}</td>
          <td>{item.tenSV}</td>
          <td>{item.sdtSV}</td>
          <td>{item.emailSV}</td>
          <td>
            <button
              onClick={() => {
                this.props.getInfoSV(index);
              }}
              className="btn btn-primary"
            >
              sửa
            </button>
            <button
              onClick={() => {
                this.props.deleteSV(index);
              }}
              className="btn btn-danger"
            >
              xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    sinhVienArr: state.formSinhVienReducer.sinhVienArr,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    deleteSV: (index) => {
      dispatch(deleteSV(index));
    },
    getInfoSV: (index) => {
      dispatch(getInfoSV(index));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TableSinhVien);
