import {
  ADD_SV,
  LAY_DL,
  DELETE_SV,
  GET_INFO_SV,
  UPDATE_SV,
} from "../type/SVType";
import {} from "../type/SVType";

export let addSV = (payload) => {
  return { type: ADD_SV, payload };
};

export let getInput = (payload) => {
  return { type: LAY_DL, payload };
};

export let deleteSV = (payload) => {
  return { type: DELETE_SV, payload };
};
export let getInfoSV = (payload) => {
  return { type: GET_INFO_SV, payload };
};
export let updateSV = (payload) => {
  return {
    type: UPDATE_SV,
    payload,
  };
};
