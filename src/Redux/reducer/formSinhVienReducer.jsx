import { initialSV } from "../../Form/utilsSinhVien";
import {
  ADD_SV,
  DELETE_SV,
  GET_INFO_SV,
  LAY_DL,
  UPDATE_SV,
} from "../type/SVType";

const initialState = {
  sinhVienArr: [],
  sinhVienInput: initialSV,
};

export let formSinhVienReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case LAY_DL: {
      let key = payload.id;
      let value = payload.value;
      let cloneSinhVienInput = { ...state.sinhVienInput, [key]: value };
      console.log(cloneSinhVienInput);
      return { ...state, sinhVienInput: cloneSinhVienInput };
    }
    case ADD_SV: {
      return {
        ...state,
        sinhVienArr: [...state.sinhVienArr, payload],
        sinhVienInput: initialSV,
      };
    }
    case DELETE_SV: {
      let clonesinhVienArr = [...state.sinhVienArr];
      clonesinhVienArr.splice(payload, 1);
      return { ...state, sinhVienArr: clonesinhVienArr };
    }
    case GET_INFO_SV: {
      let clonesinhVienInput = { ...state.sinhVienArr[payload] };

      return { ...state, sinhVienInput: clonesinhVienInput };
    }
    case UPDATE_SV: {
      let clonesinhVienArr = [...state.sinhVienArr];
      let index = clonesinhVienArr.findIndex(
        (item) => item.maSV == payload.maSV
      );
      if (index !== -1) {
        clonesinhVienArr[index] = payload;
        return {
          ...state,
          sinhVienArr: clonesinhVienArr,
          sinhVienInput: initialSV,
        };
      }
    }
    default:
      return state;
  }
};
