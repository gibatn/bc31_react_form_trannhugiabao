import { combineReducers } from "redux";
import { formSinhVienReducer } from "./formSinhVienReducer";

export let rootReducerSinhVien = combineReducers({
  formSinhVienReducer,
});
